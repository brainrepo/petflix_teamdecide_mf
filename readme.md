# Team inspire

1. Creazione del microservizio
2. Creazione del frontend

# Inizializzo il frontend

Mi sposto nella cartella del team e genero il frontend in react usando create-single-spa

    npx create-single-spa

Rispondo alle domande

    Directory for new project: frontend
    Select type to generate: single-spa application / parcel
    Which framework do you want to use?: react
    Which package manager do you want to use?: npm
    Will this project use Typescript?: (y/N)
    Organization name (use lowercase and dashes): petflix
    Project name (use lowercase and dashes): decide

Lo script geenrerà o file

    Run 'npm start -- --port 8500'

    Go to http://single-spa-playground.org/playground/instant-test?name=@petflix/decide&url=8500 to see it working!

Aprendo la pagina avremo la possibilità di testare il microfrontend del nostro team

# Sviluppo il microfrontend

Iniziamo a sviluppare il nostro frontend in react ma inseriamo anche le libs:

    npm install -P react-router-dom swr

Nello sviluppo della applicazione è importante fixare la funzione del test altrimenti non si riesce a pushare oppure pushare bypassando gli hook

    git commit --no-verify

o rimuovo dal package.json

    "husky": {
        "hooks": {
            "pre-commit": "pretty-quick --staged && concurrently npm:test npm:lint"
        }
    },

# Creo lo script di build

Inserisco nel package.json lo script che mi serve per servire l'applicazione

    "scripts": {
        ...
        "on": "webpack --mode=production && npx mfserve --listen 1001 dist",
        ...

Tutto il processo di building verrà lanciato

- creata la cartella dist con il contenuto di produzione
- esposto un semplice serverino nella porta 1001

Possiamo testare l'app provando a fare l'override con l'app appena servita

# Inglobiamo un dockerfile

Creo un dockerfil con questo contenuto

    FROM node:14
    WORKDIR /usr/src/app
    COPY package*.json ./
    RUN npm install --silent
    COPY . .
    EXPOSE 1001
    RUN npm run build

    RUN npm i -g @microfrontends/serve

    CMD ["npm", "run", "expose-prod"]

Modifico gli scripts nel package per permettere una buil e una esposizione separati in modo da poter essere usati da docker

    "scripts": {
        ...
        "on": "webpack --mode=production && npx mfserve --listen 1001 dist",
        "expose-prod": "npx mfserve --listen 1001 dist",
        ...
