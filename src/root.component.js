import React from "react";
import FilmList from "./components/FilmList";
import FilmSchede from "./components/FilmSchede";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./tailwind.css";

export default function Root(props) {
  return (
    <div className="bg-gray-900 border-8 border-blue-600">
      <Router>
        <Switch>
          <Route path="/films/:id">
            <FilmSchede></FilmSchede>
          </Route>
          <Route path="/films/">
            <FilmList></FilmList>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
