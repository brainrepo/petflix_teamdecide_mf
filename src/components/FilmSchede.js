import React from "react";
import useSWR from "swr";
import { useParams } from "react-router-dom";

import { navigateToUrl } from "single-spa";

const FilmSchede = () => {
  const { id } = useParams();
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: film } = useSWR(`http://localhost:90/${id}`, fetcher);

  return (
    <div className="bg-gray-900 flex items-center">
      <article>
        <img
          src={film?.Poster}
          alt="poster"
          className="w-full rounder-lg p-3 overflow-hidden"
        />
      </article>
      <div className="p-3 w-1/2">
        <div className="mb-3">
          <span className="text-yellow-400 text-sm font-bold">
            {film?.imdbRating} /{" "}
          </span>
          <span className="text-yellow-400 opacity-50 text-xs font-bold">
            {film?.imdbVotes} votes
          </span>
        </div>
        <div className="text-white text-xl">{film?.Title}</div>
        <div className="text-gray-700 text-sm font-bold">
          {film?.Language} - {film?.Year}
        </div>
        <div class="text-gray-500 mt-3">{film?.Plot}</div>
        <div className="text-white mt-2 text-sm">Writer: {film?.Writer}</div>
      </div>
      <div>
        <a href="/inspire" onClick={navigateToUrl}>Clicca qui per i suggerimenti</a>
      </div>
    </div>
  );
};

export default FilmSchede;
