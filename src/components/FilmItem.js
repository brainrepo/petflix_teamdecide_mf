import React from "react";
import { Link } from "react-router-dom";

const FilmItem = (props) => {
  return (
    <li>
      <Link
        to={"/films/" + props.imdbID}
        className="w-1/4 flex-col items-end h-full"
      >
        <img
          src={props.Poster}
          alt="cover"
          className="w-full rounded-lg p-3 overflow-hidden"
        />
        <div className="text-center">
          <span className="text-yellow-400 text-xs font-bold">
            {props.imdbRating} /{" "}
          </span>
          <span className="text-yellow-400 text-xs font-bold opacity-50">
            {props.imdbVotes}
          </span>
        </div>
        <div className="text-white text-center text-lg">{props.Title}</div>
        <div className="text-gray-600 text-sm font-bold text-center">
          {props.Language} - {props.Year}
        </div>
      </Link>
    </li>
  );
};

export default FilmItem;
