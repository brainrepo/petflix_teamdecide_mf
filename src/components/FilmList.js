import React from "react";

import useSWR from "swr";
import FilmItem from "./FilmItem";

const FilmList = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: films } = useSWR("http://localhost:90", fetcher);

  return (
    <div>
      <div className="p-3">
        <h3 className="text-white text-2xl font-fold text-center m-0 leading-none">
          Ultimi film su petflix
        </h3>
        <h4 className="text-gray-600 uppercase tracking-relaxed text-center">
          Scegline uno e inizia la tua esperienza!
        </h4>
      </div>
      <ul className="flex flex-wrap m-3">
        {films?.map((film) => (
          <FilmItem {...film}></FilmItem>
        ))}
      </ul>
    </div>
  );
};

export default FilmList;
